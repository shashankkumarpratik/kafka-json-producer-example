package com.onlinetutorialspoint.model;

public class Item {
	
    private String FACILITY;
    private String PRIORITY;
    private String HOST;
    private String PROGRAM;
    private String DATE;
    private String MESSAGE;

    public Item(String FACILITY, String PRIORITY, String HOST, String PROGRAM, String DATE, String MESSAGE) {
        this.FACILITY = FACILITY;
        this.PRIORITY = PRIORITY;
        this.HOST = HOST;
        this.PROGRAM = PROGRAM;
        this.DATE = DATE;
        this.MESSAGE = MESSAGE;
    }

    public String getFACILITY() {
		return FACILITY;
	}
	public void setFACILITY(String fACILITY) {
		FACILITY = fACILITY;
	}
	public String getPRIORITY() {
		return PRIORITY;
	}
	public void setPRIORITY(String pRIORITY) {
		PRIORITY = pRIORITY;
	}
	public String getHOST() {
		return HOST;
	}
	public void setHOST(String hOST) {
		HOST = hOST;
	}
	public String getPROGRAM() {
		return PROGRAM;
	}
	public void setPROGRAM(String pROGRAM) {
		PROGRAM = pROGRAM;
	}
	public String getDATE() {
		return DATE;
	}
	public void setDATE(String dATE) {
		DATE = dATE;
	}
	public String getMESSAGE() {
		return MESSAGE;
	}
	public void setMESSAGE(String mESSAGE) {
		MESSAGE = mESSAGE;
	}
}
